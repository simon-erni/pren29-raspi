/*
 * PREN29
 * 
 */
package ch.hslu.pren29.client;

import ch.hslu.pren29.config.Config;
import ch.hslu.pren29.hardware.camera.RecognitionConfiguration;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.rmi.RemoteException;

/**
 *
 * @author Simon
 */
public class ConfigDummy implements Config {

    private RecognitionConfiguration config;

    private JFileChooser chooser;

    public ConfigDummy() {
        config = new RecognitionConfiguration();
        chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "Images", "jpg", "jpeg", "png");
        chooser.setFileFilter(filter);
    }

    @Override
    public RecognitionConfiguration getCurrentConfiguration() throws RemoteException {
        return config;
    }

    @Override
    public void setConfiguration(RecognitionConfiguration conf) throws RemoteException {
        this.config = conf;
    }

    @Override
    public byte[] takeImage() throws RemoteException {

        int returnVal = chooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                File imageFile = chooser.getSelectedFile();
                ImageIO.write(ImageIO.read(imageFile), "jpg", baos);
                baos.flush();
                return baos.toByteArray();
            } catch (Exception ex) {
                throw new RemoteException(ex.getMessage());
            }

        } else {
            throw new RemoteException("No File chosen");
        }

    }

}
