package ch.hslu.pren29.client;

import ch.hslu.pren29.config.Config;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by Simon on 12.06.2015.
 */
public class SaveImage {

    public static void main(String[] args) {
        try {
            String name = "config";
            Registry registry = LocateRegistry.getRegistry("192.168.29.1");
            Config config = (Config) registry.lookup(name);





            byte[] byteArray = config.takeImage();
            InputStream in = new ByteArrayInputStream(byteArray);
            BufferedImage image = ImageIO.read(in);

            File outputfile = new File("mit schwarz_links.png");
            ImageIO.write(image, "png", outputfile);



        } catch (Exception e) {
            System.err.println("Connect exception:");
            e.printStackTrace();
        }
    }
}
