package ch.hslu.pren29.hardware.fake;

import ch.hslu.pren29.hardware.camera.Camera;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.logging.Logger;

/**
 * Created by Simon on 04.06.2015.
 */
public class FakeCamera extends Camera {

    @Inject
    protected FakeCamera(Logger logger, ListeningExecutorService executorService) {
        super(logger, executorService);
    }

    @Override
    public BufferedImage takeImage() throws Exception {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        
        return ImageIO.read(new File("middle.jpeg"));

    }
}
