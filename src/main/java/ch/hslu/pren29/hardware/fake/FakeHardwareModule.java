package ch.hslu.pren29.hardware.fake;

import ch.hslu.pren29.hardware.camera.Camera;
import ch.hslu.pren29.hardware.camera.RecognitionAlgorithm;
import ch.hslu.pren29.hardware.camera.RecognitionConfiguration;
import ch.hslu.pren29.hardware.communication.PwmOut;
import ch.hslu.pren29.hardware.components.Direction;
import ch.hslu.pren29.hardware.components.Hopper;
import ch.hslu.pren29.hardware.components.Latch;
import ch.hslu.pren29.hardware.components.Motor;
import ch.hslu.pren29.hardware.components.PositionSensor;
import ch.hslu.pren29.hardware.components.StatusLEDs;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.pi4j.io.gpio.GpioController;

import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 * Registers all needed Classes for the HardwareComponents.
 */
public class FakeHardwareModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(Latch.class);
        bind(Hopper.class);
        bind(PositionSensor.class);
        bind(StatusLEDs.class);
        bind(Motor.class);
        bind(Direction.class);

        bind(PwmOut.class);

        bind(RecognitionConfiguration.class);
        bind(Camera.class).to(FakeCamera.class);
        bind(RecognitionAlgorithm.class);
        bind(GpioController.class).to(FakeGpioController.class);
    }

    @Provides
    @Singleton
    public Writer providePiBlasterWriter() {
        return new OutputStreamWriter(System.out);
    }
}
