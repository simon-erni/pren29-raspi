package ch.hslu.pren29.hardware.fake;

import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.GpioPinShutdown;
import com.pi4j.io.gpio.GpioProvider;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinMode;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.event.GpioPinListener;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;


public class FakeGpioOutputPin implements GpioPinDigitalOutput {

    private final Pin pin;
    private boolean high;


    public FakeGpioOutputPin(Pin pin) {
        this.pin = pin;

    }


    @Override
    public void high() {
        high = true;
        System.out.println(pin.getName() + " high");
    }

    @Override
    public void low() {
        high = false;
        System.out.println(pin.getName() + " low");

    }

    @Override
    public void toggle() {
        high = !high;
    }

    @Override
    public Future<?> blink(long l) {
        return null;
    }

    @Override
    public Future<?> blink(long l, PinState pinState) {
        return null;
    }

    @Override
    public Future<?> blink(long l, long l1) {
        return null;
    }

    @Override
    public Future<?> blink(long l, long l1, PinState pinState) {
        return null;
    }

    @Override
    public Future<?> pulse(long l) {
        return null;
    }

    @Override
    public Future<?> pulse(long l, Callable<Void> callable) {
        return null;
    }

    @Override
    public Future<?> pulse(long l, boolean b) {
        return null;
    }

    @Override
    public Future<?> pulse(long l, boolean b, Callable<Void> callable) {
        return null;
    }

    @Override
    public Future<?> pulse(long l, PinState pinState) {
        return null;
    }

    @Override
    public Future<?> pulse(long l, PinState pinState, Callable<Void> callable) {
        return null;
    }

    @Override
    public Future<?> pulse(long l, PinState pinState, boolean b) {
        return null;
    }

    @Override
    public Future<?> pulse(long l, PinState pinState, boolean b, Callable<Void> callable) {
        return null;
    }

    @Override
    public void setState(PinState pinState) {

    }

    @Override
    public void setState(boolean b) {

    }

    @Override
    public boolean isHigh() {
        return high;
    }

    @Override
    public boolean isLow() {
        return !high;
    }

    @Override
    public PinState getState() {
        return null;
    }

    @Override
    public boolean isState(PinState pinState) {
        return false;
    }

    @Override
    public GpioProvider getProvider() {
        return null;
    }

    @Override
    public Pin getPin() {
        return null;
    }

    @Override
    public void setName(String s) {

    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void setTag(Object o) {

    }

    @Override
    public Object getTag() {
        return null;
    }

    @Override
    public void setProperty(String s, String s1) {

    }

    @Override
    public boolean hasProperty(String s) {
        return false;
    }

    @Override
    public String getProperty(String s) {
        return null;
    }

    @Override
    public String getProperty(String s, String s1) {
        return null;
    }

    @Override
    public Map<String, String> getProperties() {
        return null;
    }

    @Override
    public void removeProperty(String s) {

    }

    @Override
    public void clearProperties() {

    }

    @Override
    public void export(PinMode pinMode) {

    }

    @Override
    public void export(PinMode pinMode, PinState pinState) {

    }

    @Override
    public void unexport() {

    }

    @Override
    public boolean isExported() {
        return false;
    }

    @Override
    public void setMode(PinMode pinMode) {

    }

    @Override
    public PinMode getMode() {
        return null;
    }

    @Override
    public boolean isMode(PinMode pinMode) {
        return false;
    }

    @Override
    public void setPullResistance(PinPullResistance pinPullResistance) {

    }

    @Override
    public PinPullResistance getPullResistance() {
        return null;
    }

    @Override
    public boolean isPullResistance(PinPullResistance pinPullResistance) {
        return false;
    }

    @Override
    public Collection<GpioPinListener> getListeners() {
        return null;
    }

    @Override
    public void addListener(GpioPinListener... gpioPinListeners) {

    }

    @Override
    public void addListener(List<? extends GpioPinListener> list) {

    }

    @Override
    public boolean hasListener(GpioPinListener... gpioPinListeners) {
        return false;
    }

    @Override
    public void removeListener(GpioPinListener... gpioPinListeners) {

    }

    @Override
    public void removeListener(List<? extends GpioPinListener> list) {

    }

    @Override
    public void removeAllListeners() {

    }

    @Override
    public GpioPinShutdown getShutdownOptions() {
        return null;
    }

    @Override
    public void setShutdownOptions(GpioPinShutdown gpioPinShutdown) {

    }

    @Override
    public void setShutdownOptions(Boolean aBoolean) {

    }

    @Override
    public void setShutdownOptions(Boolean aBoolean, PinState pinState) {

    }

    @Override
    public void setShutdownOptions(Boolean aBoolean, PinState pinState, PinPullResistance pinPullResistance) {

    }

    @Override
    public void setShutdownOptions(Boolean aBoolean, PinState pinState, PinPullResistance pinPullResistance, PinMode pinMode) {

    }
}
