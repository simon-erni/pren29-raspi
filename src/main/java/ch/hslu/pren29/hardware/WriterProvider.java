package ch.hslu.pren29.hardware;

import com.google.inject.throwingproviders.CheckedProvider;

import java.io.IOException;


public interface WriterProvider<Writer> extends CheckedProvider<Writer> {
    Writer get() throws IOException;
}