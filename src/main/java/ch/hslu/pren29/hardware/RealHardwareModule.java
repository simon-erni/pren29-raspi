package ch.hslu.pren29.hardware;

import ch.hslu.pren29.hardware.camera.Camera;
import ch.hslu.pren29.hardware.camera.RecognitionAlgorithm;
import ch.hslu.pren29.hardware.camera.RecognitionConfiguration;
import ch.hslu.pren29.hardware.communication.PwmOut;
import ch.hslu.pren29.hardware.components.Direction;
import ch.hslu.pren29.hardware.components.Hopper;
import ch.hslu.pren29.hardware.components.Latch;
import ch.hslu.pren29.hardware.components.Motor;
import ch.hslu.pren29.hardware.components.PositionSensor;
import ch.hslu.pren29.hardware.components.StatusLEDs;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 * Registers all needed Classes for the HardwareComponents.
 */
public class RealHardwareModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(Latch.class);
        bind(Hopper.class);
        bind(PositionSensor.class);
        bind(StatusLEDs.class);
        bind(Motor.class);
        bind(Direction.class);

        bind(RecognitionConfiguration.class);
        bind(Camera.class);
        bind(RecognitionAlgorithm.class);

        bind(PwmOut.class);

    }

    @Provides
    @Singleton
    public GpioController provideGpioController() {
        return GpioFactory.getInstance();
    }

    @Provides
    @Singleton
    public Writer providePiBlasterWriter() throws IOException {
            return new OutputStreamWriter(new FileOutputStream("/dev/pi-blaster"), "ASCII");
    }


}
