package ch.hslu.pren29.hardware.camera.exception;

/**
 * Thrown when the Basket hasn't been found.
 */
public class BasketNotFoundException extends Exception {

}
