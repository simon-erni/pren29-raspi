/*
 * PREN29
 * 
 */
package ch.hslu.pren29.hardware.camera;

import com.google.inject.Singleton;

import java.io.Serializable;
import java.util.prefs.Preferences;

/**
 * @author Simon
 */
@Singleton
public class RecognitionConfiguration implements Serializable {

    private static final Preferences prefs = Preferences.userNodeForPackage(RecognitionConfiguration.class);

    private static final String MIN_KORB_WIDTH = "korb_width";
    private static final String MAX_KORB_WIDTH = "korb_width_max";
    private static final String KORB_LUMINANCE = "korb_luminance";
    private static final String PIXEL_ROW = "pixel_row";
    private static final String EDGE_HEIGHT = "edge_height";
    private static final String EDGE_DETECTING_WIDTH = "edge_detecting_width";

    private int pixelRow, minKorbWidth, maxKorbWidth, maxKorbLuminancy, edgeHeight, edgeDetectingWidth;

    public RecognitionConfiguration() {
        this.pixelRow = prefs.getInt(PIXEL_ROW, 400);
        this.minKorbWidth = prefs.getInt(MIN_KORB_WIDTH, 300);
        this.maxKorbWidth = prefs.getInt(MAX_KORB_WIDTH, 350);
        this.maxKorbLuminancy = prefs.getInt(KORB_LUMINANCE, 80);
        this.edgeHeight = prefs.getInt(EDGE_HEIGHT, 30);
        this.edgeDetectingWidth = prefs.getInt(EDGE_DETECTING_WIDTH, 5);
    }

    private void persist() {
        prefs.putInt(MIN_KORB_WIDTH, minKorbWidth);
        prefs.putInt(MAX_KORB_WIDTH, maxKorbWidth);
        prefs.putInt(PIXEL_ROW, pixelRow);
        prefs.putInt(KORB_LUMINANCE, maxKorbLuminancy);
        prefs.putInt(EDGE_HEIGHT, edgeHeight);
        prefs.putInt(EDGE_DETECTING_WIDTH, edgeDetectingWidth);
    }

    public int getPixelRow() {
        return pixelRow;
    }

    public void setPixelRow(int pixelRow) {
        this.pixelRow = pixelRow;
        persist();
    }

    public int getMinKorbWidth() {
        return minKorbWidth;
    }

    public void setMinKorbWidth(int minKorbWidth) {
        this.minKorbWidth = minKorbWidth;
        persist();
    }

    public int getMaxKorbLuminancy() {
        return maxKorbLuminancy;
    }

    public void setMaxKorbLuminancy(int maxKorbLuminancy) {
        this.maxKorbLuminancy = maxKorbLuminancy;
        persist();
    }

    public int getEdgeHeight() {
        return edgeHeight;
    }

    public void setEdgeHeight(int edgeHeight) {
        this.edgeHeight = edgeHeight;
        persist();
    }

    public int getEdgeDetectingWidth() {
        return edgeDetectingWidth;
    }

    public void setEdgeDetectingWidth(int edgeDetectingWidth) {
        this.edgeDetectingWidth = edgeDetectingWidth;
        persist();
    }

    public int getMaxKorbWidth() {
        return maxKorbWidth;
    }

    public void setMaxKorbWidth(int maxKorbWidth) {
        this.maxKorbWidth = maxKorbWidth;
        persist();
    }

    public void refresh(RecognitionConfiguration conf) {
        this.setMaxKorbLuminancy(conf.getMaxKorbLuminancy());
        this.setMinKorbWidth(conf.getMinKorbWidth());
        this.setPixelRow(conf.getPixelRow());
        this.setEdgeDetectingWidth(conf.getEdgeDetectingWidth());
        this.setMaxKorbWidth(conf.getMaxKorbWidth());
        this.setEdgeHeight(conf.getEdgeHeight());
    }

    @Override
    public String toString() {
        return "RecognitionConfiguration{" +
                "pixelRow=" + pixelRow +
                ", minKorbWidth=" + minKorbWidth +
                ", maxKorbWidth=" + maxKorbWidth +
                ", maxKorbLuminancy=" + maxKorbLuminancy +
                ", edgeHeight=" + edgeHeight +
                ", edgeDetectingWidth=" + edgeDetectingWidth +
                '}';
    }
}
