package ch.hslu.pren29.hardware.camera;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 *
 * @author Simon
 */
public class KorbLocation {

    private int horizontalStart;
    private int horizontalEnd;


    public KorbLocation(int horizontalStart, int horizontalEnd) {
        this.horizontalStart = horizontalStart;
        this.horizontalEnd = horizontalEnd;
    }

    public int getHorizontalStart() {
        return horizontalStart;
    }

    public int getHorizontalEnd() {
        return horizontalEnd;
    }

    public int getHorizontalMitte() {
        return (horizontalEnd - horizontalStart) / 2 + horizontalStart;
    }

    public BufferedImage drawOnImage(BufferedImage image) {
        Graphics g2d = image.createGraphics();
        g2d.setColor(Color.red);
        g2d.drawLine(getHorizontalStart(), image.getHeight(), getHorizontalStart(), 0);
        g2d.drawLine(getHorizontalEnd(), image.getHeight(), getHorizontalEnd(), 0);

        return image;
    }

    @Override
    public String toString() {
        String returnString = "";
        returnString += "Horizontal Start: " + horizontalStart + "\n";
        returnString += "Horizontal End: " + horizontalEnd + "\n";
        return returnString;
    }

}
