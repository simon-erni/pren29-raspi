package ch.hslu.pren29.hardware.camera.exception;

/**
 * Created by Simon on 31.05.2015.
 */
public class CameraException extends Exception {
    public CameraException(String message) {
        super(message);
    }
}
