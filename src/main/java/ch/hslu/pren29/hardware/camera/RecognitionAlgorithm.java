package ch.hslu.pren29.hardware.camera;

import ch.hslu.pren29.hardware.camera.exception.BasketNotFoundException;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;

import java.awt.image.BufferedImage;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Simon
 */
public class RecognitionAlgorithm {

    private final ListeningExecutorService executorService;
    private final RecognitionConfiguration recognitionConfiguration;
    private final Logger logger;

    @Inject
    public RecognitionAlgorithm(Logger logger, ListeningExecutorService executorService, RecognitionConfiguration recognitionConfiguration) {
        this.executorService = executorService;
        this.recognitionConfiguration = recognitionConfiguration;
        this.logger = logger;
    }

    public KorbLocation recognize(BufferedImage img) throws BasketNotFoundException {
        return getKorbLocation(getPixelData(recognitionConfiguration.getPixelRow(), img));
    }

    public ListenableFuture<Double> getAngle(final BufferedImage img) throws BasketNotFoundException {
        return executorService.submit(new Callable<Double>() {
            @Override
            public Double call() throws Exception {
                KorbLocation location = recognize(img);
                return pixelToAngle(location.getHorizontalMitte(), img.getWidth());
            }
        });
    }

    /**
     * ToDo: Auto Retry with other parameters.
     *
     * @param pixelData The Pixel Datal.
     * @return The KorbLocation.
     * @throws Exception If the Basket couldn't be found.
     */
    public KorbLocation getKorbLocation(int[] pixelData) throws BasketNotFoundException {

        int startPixel = searchStartPixel(pixelData);
        int endPixel = searchEndPixel(pixelData, startPixel);

        if (startPixel == 0) {
            startPixel = endPixel - recognitionConfiguration.getMaxKorbWidth();
        }

        if (endPixel - startPixel > recognitionConfiguration.getMaxKorbWidth()) {
            int difference = Math.abs(endPixel - startPixel) - recognitionConfiguration.getMaxKorbWidth();
            difference = Math.round(difference / 2);
            startPixel += difference;
            endPixel -= difference;
        }

        return new KorbLocation(startPixel, endPixel);
    }


    private int searchStartPixel(int[] pixelData) throws BasketNotFoundException {

        for (int i = 0; i < pixelData.length; i++) {

            if (pixelData[i] <= recognitionConfiguration.getMaxKorbLuminancy()) {

                if (averageLuminancy(pixelData, i, i + recognitionConfiguration.getMinKorbWidth()) <= recognitionConfiguration.getMaxKorbLuminancy()) {
                    return i;
                }

            }

            int edgePos = i + recognitionConfiguration.getEdgeDetectingWidth();

            if (edgePos < pixelData.length) {
                if ((pixelData[i] - pixelData[edgePos]) > recognitionConfiguration.getEdgeHeight()) {
                    if (averageLuminancy(pixelData, edgePos, edgePos + recognitionConfiguration.getMinKorbWidth()) <= recognitionConfiguration.getMaxKorbLuminancy()) {
                        return edgePos;
                    }
                }
            }
        }

        logger.log(Level.WARNING, "Couldn't find start Position. Current values:" + recognitionConfiguration.toString());

        throw new BasketNotFoundException();
    }

    private int searchEndPixel(int[] pixelData, final int startPixel) {

        if (startPixel + recognitionConfiguration.getMaxKorbWidth() < pixelData.length) {

            for (int i = startPixel + recognitionConfiguration.getMinKorbWidth(); i < pixelData.length; i++) {


                if (pixelData[i] > recognitionConfiguration.getMaxKorbLuminancy()) {

                    if (averageLuminancy(pixelData, i - recognitionConfiguration.getMinKorbWidth(), i) <= recognitionConfiguration.getMaxKorbLuminancy()) {
                        return i;
                    }

                }

                int edgePos = i + recognitionConfiguration.getEdgeDetectingWidth();
                if (edgePos < pixelData.length) {
                    if ((pixelData[edgePos] - pixelData[i]) > recognitionConfiguration.getEdgeHeight()) {
                        return edgePos;
                    }
                }

            }
        }

        return startPixel + recognitionConfiguration.getMaxKorbWidth();

    }

    private int averageLuminancy(int[] pixelData, int start, int end) {
        if (start < 0) {
            start = 0;
        }
        if (end < 0) {
            end = 0;
        }
        if (end > pixelData.length) {
            end = pixelData.length;
        }
        if (start > pixelData.length) {
            start = pixelData.length;
        }

        int distance = Math.abs(end - start);

        if (distance == 0) {
            return pixelData[start];
        }

        long counter = 0;

        for (int i = start; i < end; i++) {
            counter += pixelData[i];
        }

        int luminancy = Math.round(counter / distance);
        return luminancy;
    }

    private int[] getPixelData(int row, BufferedImage img) {

        int[] pixelData = new int[img.getWidth()];
        //Get the Row of Pixels
        for (int i = 0; i < pixelData.length; i++) {

            int[] currentPixel = getRGBArray(i, row, img);

            pixelData[i] = currentPixel[0] + currentPixel[1] + currentPixel[2];
        }

        return pixelData;
    }

    private int[] getRGBArray(int x, int y, BufferedImage img) {
        int argb = img.getRGB(x, y);

        int rgb[] = new int[]{
                (argb >> 16) & 0xff, //red
                (argb >> 8) & 0xff, //green
                (argb) & 0xff //blue
        };
        return rgb;
    }

    public double pixelToAngle(int pixel, int pixelWidth) {

        pixel = pixel - (pixelWidth / 2);

        return 0 - Math.toDegrees(Math.atan((((double) pixel) * 0.003) / 3.3d));
    }
}
