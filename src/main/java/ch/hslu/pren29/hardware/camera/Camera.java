package ch.hslu.pren29.hardware.camera;

import ch.hslu.pren29.hardware.camera.exception.CameraException;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Simon
 */
@Singleton
public class Camera {

    private final String imagePath = "image.jpeg";
    private final Logger logger;
    private final ListeningExecutorService executorService;

    @Inject
    protected Camera(Logger logger, ListeningExecutorService executorService) {
        this.logger = logger;
        this.executorService = executorService;
    }

    private BufferedImage readImageFromFile() throws IOException {
        File imageFile = new File(imagePath);
        logger.log(Level.FINEST, "Reading File from " + imageFile.getAbsolutePath());
        return ImageIO.read(imageFile);
    }

    private void saveImageFromWebcam() throws IOException, InterruptedException, CameraException {

        Runtime rt = Runtime.getRuntime();

        Process p = rt.exec("streamer -s 640x480 -w 1 -f jpeg -o " + imagePath);
        int returnValue = p.waitFor();
        if (returnValue != 0) {
            throw new CameraException("Error accessing Camera, Return Value of Streamer Process: " + returnValue);
        }
    }


    public synchronized BufferedImage takeImage() throws Exception {

        saveImageFromWebcam();

        return readImageFromFile();

    }
}
