/*
 * PREN29
 * 
 */
package ch.hslu.pren29.hardware.components;

import ch.hslu.pren29.hardware.communication.PwmOut;
import ch.hslu.pren29.util.TimeUtils;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Simon
 */
@Singleton
public class Hopper {

    private final PwmOut pwmOut;

    private final ListeningExecutorService executorService;

    private final TimeUtils timeUtils;

    private final static int HOPPER_PIN = 18;

    private final Logger logger;

    @Inject
    public Hopper(Logger logger, PwmOut pwmOut, TimeUtils timeUtils, ListeningExecutorService executorService) {
        this.pwmOut = pwmOut;
        this.executorService = executorService;
        this.timeUtils = timeUtils;
        this.logger = logger;

        initHopper();

    }

    /**
     * ToDo: Check if it can be removed.
     */
    private void initHopper() {
        pwmOut.pwmOut(HOPPER_PIN, 0.2f);
        pwmOut.pwmOut(HOPPER_PIN, 0);
    }

    /**
     * Loads a ball into the machine.
     *
     * @return null, when done
     * @throws Exception if unable to compute a result
     */
    public ListenableFuture<Void> loadBall() {

        return executorService.submit(new Callable<Void>() {
            @Override
            public Void call() throws Exception {

                logger.log(Level.WARNING, "Loading Ball");

                pwmOut.pwmOut(HOPPER_PIN, 0.01f);
                pwmOut.pwmOut(HOPPER_PIN, 0.01f);

                timeUtils.wait(800);

                pwmOut.pwmOut(HOPPER_PIN, 0.15f);
                pwmOut.pwmOut(HOPPER_PIN, 0.15f);

                timeUtils.wait(700);

                pwmOut.pwmOut(HOPPER_PIN, 0);
                pwmOut.pwmOut(HOPPER_PIN, 0);

                timeUtils.wait(200);

                pwmOut.releasePWM(HOPPER_PIN);

                timeUtils.wait(600);

                return null;
            }
        });
    }
}
