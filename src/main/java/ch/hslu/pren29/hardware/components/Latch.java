/*
 * PREN29
 * 
 */
package ch.hslu.pren29.hardware.components;

import ch.hslu.pren29.hardware.communication.PwmOut;
import ch.hslu.pren29.util.TimeUtils;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.concurrent.Callable;

/**
 * Steuert die Öffnung der Klappe.
 *
 * @author Simon
 */
@Singleton
public class Latch {

    private final PwmOut hardware;

    private final ListeningExecutorService executorService;

    private final static int LATCH_PIN = 4;

    private final TimeUtils timeUtils;

    @Inject
    public Latch(PwmOut hardware, TimeUtils timeUtils, ListeningExecutorService executorService) {
        this.hardware = hardware;
        this.executorService = executorService;
        this.timeUtils = timeUtils;
    }


    /**
     * Closes the Latch.
     *
     * @return ListenAble Future when done.
     */
    public ListenableFuture<Void> close() {
        return executorService.submit(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                hardware.pwmOut(LATCH_PIN, 0.243f);
                hardware.pwmOut(LATCH_PIN, 0.243f);
                timeUtils.wait(400);
                return null;
            }
        });
    }

    /**
     * Opens the Latch.
     *
     * @return Future when done.
     */
    public ListenableFuture<Void> open() {
        return executorService.submit(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                hardware.pwmOut(LATCH_PIN, 0.16f);
                hardware.pwmOut(LATCH_PIN, 0.16f);
                timeUtils.wait(400);
                return null;
            }
        });
    }

}
