/*
 * PREN29
 * 
 */
package ch.hslu.pren29.hardware.components;

import ch.hslu.pren29.hardware.communication.PwmOut;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Simon
 */
@Singleton
public class Motor {

    private final PwmOut pwmOut;

    private final static int DC_MOTOR_PIN = 24;
    private final GpioPinDigitalOutput motorBreakPin;

    private final Logger logger;

    private int currentPercentage;

    @Inject
    protected Motor(Logger logger, PwmOut pwmOut, GpioController gpioController) {
        this.pwmOut = pwmOut;
        this.logger = logger;
        motorBreakPin = gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_23);

        currentPercentage = 0;
        motorBreakPin.high();
    }

    /**
     * Sets the motor to the desired percentage. Returns immediately.
     *
     * @param percentage 0 equals motor stopping, 100 equals full power.
     */
    public void setPercentage(int percentage) {
        if (0 <= percentage && percentage <= 100) {

            if (percentage != currentPercentage) {
                logger.log(Level.WARNING, "Setting Motor to percentage " + percentage);
                pwmOut.pwmOut(DC_MOTOR_PIN, percentage / 100.0F);
                pwmOut.pwmOut(DC_MOTOR_PIN, percentage / 100.0F);

                if (percentage == 0) {
                    pwmOut.releasePWM(DC_MOTOR_PIN);
                }
                currentPercentage = percentage;
            }
        } else {
            throw new IllegalArgumentException("Invalid Percentage: " + percentage);
        }
    }
}
