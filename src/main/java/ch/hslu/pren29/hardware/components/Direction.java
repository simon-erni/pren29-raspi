/*
 * PREN29
 * 
 */
package ch.hslu.pren29.hardware.components;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;

import java.util.concurrent.Callable;

/**
 * @author Simon
 */
@Singleton
public class Direction {

    private final GpioPinDigitalOutput stepperPin;

    private final GpioPinDigitalOutput stepperDirection;

    private final GpioPinDigitalOutput stepperPower;

    private long currentSteps;

    private boolean currentStepperPower;

    private final static double STEP_FACTOR = 106.666666666666666666666;

    private final ListeningExecutorService executorService;

    private ListenableFuture<Void> currentExecution;


    @Inject
    public Direction(ListeningExecutorService executorService, GpioController gpioController) {
        this.executorService = executorService;

        stepperPin = gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_29);
        stepperDirection = gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_22);
        stepperPower = gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_14);

        this.currentSteps = 0;

        stepperDirection.high();
        stepperPower.high();

        currentStepperPower = false;
    }

    public void stepperPower(boolean on) {
        if (on != currentStepperPower) {
            currentStepperPower = on;

            if (on) {
                stepperPower.low();
            } else {
                stepperPower.high();
            }
        }
    }

    private void setStepperDirection(boolean direction) {
        if (direction) {
            if (!stepperDirection.isHigh()) {
                stepperDirection.high();
            }

        } else {
            if (!stepperDirection.isLow()) {
                stepperDirection.low();
            }
        }

    }

    /**
     * Dreht den Turm auf die entsprechende Stelle. Nimmt an, dass der Turm
     * initial auf 0 steht, d.h. perfekt geradeaus schaut.
     *
     * @param angle The new angle.
     */
    public ListenableFuture<Void> setAngle(double angle) {

        final Long desiredSteps = Math.round(angle * STEP_FACTOR);

        if (currentExecution != null && !currentExecution.isDone()) {
            currentExecution.cancel(true);
        }

        return this.currentExecution = this.executorService.submit(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                goToStep(desiredSteps);
                return null;
            }
        });

    }

    public double getCurrentAngle() {
        return Math.round((double) currentSteps / STEP_FACTOR);
    }


    private void goToStep(long desiredSteps) {

        if (desiredSteps - currentSteps > 0) {
            setStepperDirection(false);
            while (currentSteps != desiredSteps && !Thread.currentThread().isInterrupted()) {
                currentSteps++;
                stepperPin.high();
                stepperPin.low();
            }
        } else {
            setStepperDirection(true);
            while (currentSteps != desiredSteps && !Thread.currentThread().isInterrupted()) {
                currentSteps--;
                stepperPin.high();
                stepperPin.low();
            }
        }

    }

}
