/*
 * PREN29
 * 
 */
package ch.hslu.pren29.hardware.components;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import java.util.logging.Logger;

/**
 * Wehn at pos immediate return
 *
 * @author Simon
 */
@Singleton
public class PositionSensor implements GpioPinListenerDigital {

    private final Logger logger;

    private boolean atPosition;

    private SettableFuture<Void> waitingForPosition;

    private SettableFuture<Void> waitingForNoPosition;


    @Inject
    public PositionSensor(GpioController gpioController, Logger logger) {

        GpioPinDigitalInput positionSensor = gpioController.provisionDigitalInputPin(RaspiPin.GPIO_21, PinPullResistance.PULL_UP);

        positionSensor.addListener(this);

        this.atPosition = positionSensor.isLow();

        this.logger = logger;

        this.waitingForPosition = SettableFuture.create();
        this.waitingForNoPosition = SettableFuture.create();
    }


    /**
     * @param gpdsce The Change Event.
     */
    @Override
    public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent gpdsce) {

        if (gpdsce.getState().isLow()) {
            logger.fine(System.currentTimeMillis() + ": At Position");
            updatePosition(true);
        } else {
            logger.fine(System.currentTimeMillis() + ": Not at Position");
            updatePosition(false);
        }

    }

    private void updatePosition(boolean newPosition) {
        if (newPosition) {
            waitingForPosition.set(null);
            if (waitingForNoPosition.isDone()) {
                waitingForNoPosition = SettableFuture.create();
            }
        } else {
            waitingForNoPosition.set(null);
            if (waitingForPosition.isDone()) {
                waitingForPosition = SettableFuture.create();
            }
        }
    }

    public ListenableFuture<Void> waitForPosition() {
        return waitingForPosition;
    }

    public ListenableFuture<Void> waitForNoPosition() {
        return waitingForNoPosition;
    }


}
