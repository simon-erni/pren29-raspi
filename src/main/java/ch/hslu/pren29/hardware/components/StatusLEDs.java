/*
 * PREN29
 * 
 */
package ch.hslu.pren29.hardware.components;

import ch.hslu.pren29.util.TimeUtils;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;

import java.util.concurrent.Callable;

/**
 * @author Simon
 */
@Singleton
public class StatusLEDs {

    private final GpioPinDigitalOutput readyLED;

    private final GpioPinDigitalOutput errorLED;

    private final GpioPinDigitalOutput runningLED;

    private final ListeningExecutorService executorService;

    private final TimeUtils timeUtils;

    @Inject
    public StatusLEDs(GpioController gpioController, ListeningExecutorService executorService, TimeUtils timeUtils) {

        readyLED = gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_02);

        runningLED = gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_09);

        errorLED = gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_08);

        this.executorService = executorService;
        this.timeUtils = timeUtils;

        initLEDs();
    }

    private void initLEDs() {
        readyLED.high();
        errorLED.low();
        runningLED.low();
    }

    public void setReadyLED(boolean state) {
        if (state) {
            readyLED.high();
        } else {
            readyLED.low();
        }
    }

    public void setErrorLED(boolean state) {
        if (state) {
            errorLED.high();
        } else {
            errorLED.low();
        }
    }

    public void blinkError(final int count) {
        executorService.submit(new Callable<Void>() {
                                   @Override
                                   public Void call() throws Exception {
                                       for (int i = 0; i < count; i++) {
                                           setErrorLED(true);
                                           timeUtils.wait(500);
                                           setErrorLED(false);
                                           timeUtils.wait(500);
                                       }
                                       return null;
                                   }
                               }
        );
    }

    public void setRunningLED(boolean state) {
        if (state) {
            runningLED.high();
        } else {
            runningLED.low();
        }
    }

}
