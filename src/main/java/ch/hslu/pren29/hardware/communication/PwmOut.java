/*
 * PREN29
 * 
 */
package ch.hslu.pren29.hardware.communication;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.io.IOException;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Simon
 */
@Singleton
public class PwmOut {

    private final Writer writer;
    private final Logger logger;

    @Inject
    public PwmOut(Logger logger, Writer writer) {
        this.writer = writer;
        this.logger = logger;

    }


    /**
     * Writes a PWM signal to a defined pin.
     *
     * @param pin
     * @param value
     */
    public synchronized void pwmOut(int pin, float value) {
        if (0 <= value && value <= 1 && isValidPin(pin)) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(pin);
            stringBuilder.append("=");
            if (value == 0) {
                stringBuilder.append("0");
            } else {
                stringBuilder.append(value);
            }
            stringBuilder.append("\n");

            writeToPWM(stringBuilder.toString());

        } else {
            throw new IllegalArgumentException("Wert: " + value + " Pin: " + pin + "\n" + "Valide Werte: 0 - 1");
        }
    }

    private boolean isValidPin(int pin) {
        switch (pin) {
            case 4:
            case 17:
            case 18:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
                return true;
            default:
                return false;
        }
    }

    public synchronized void releasePWM(int pin) {

        if (isValidPin(pin)) {
            //writeToPWM("release " + pin);

        } else {
            throw new IllegalArgumentException("Invalid Pin to be released: " + pin + "\n");

        }
    }

    private void writeToPWM(String message) {
        try {
            logger.log(Level.WARNING, message);
            writer.write(message);
            writer.flush();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Error writing to Pi-Blaster", ex);
        }
    }
}
