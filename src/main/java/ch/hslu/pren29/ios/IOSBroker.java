package ch.hslu.pren29.ios;

import ch.hslu.pren29.controlling.process.MainProcess;
import ch.hslu.pren29.controlling.tasks.Reload;
import ch.hslu.pren29.controlling.tasks.TestShoot;
import ch.hslu.pren29.hardware.components.Direction;
import ch.hslu.pren29.hardware.components.Latch;
import ch.hslu.pren29.hardware.components.Motor;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.concurrent.ExecutorService;

@Singleton
public class IOSBroker implements IOSMessageListener {

    private final IOSCommunication iosCommunication;
    private final MainProcess mainProcess;
    private final Motor motor;
    private final Latch latch;
    private final Direction direction;
    private final ExecutorService executorService;
    private final Reload reload;
    private final TestShoot testShoot;

    @Inject
    public IOSBroker(IOSCommunication iosCommunication, MainProcess mainProcess, Motor motor, Latch latch, Direction direction, ListeningExecutorService executorService, Reload reload, TestShoot testShoot) {
        this.iosCommunication = iosCommunication;
        this.motor = motor;
        this.latch = latch;
        this.direction = direction;
        this.executorService = executorService;
        this.testShoot = testShoot;
        this.iosCommunication.addMessageListener(this);
        this.mainProcess = mainProcess;
        this.reload = reload;
    }


    @Override
    public void messageReceived(String message) {

        String commandString = getCommand(message);
        double parameter = getParameter(message);

        executeCommand(commandString, parameter);

    }

    private void executeCommand(String command, double parameter) {
        switch (command) {
            case "Vorbereiten":
                mainProcess.prepare();
                return;
            case "Shoot":
                addFinishedListener(mainProcess.shoot(Math.toIntExact(Math.round(parameter))));
                return;
            case "Motor":
                motor.setPercentage(Math.toIntExact(Math.round(parameter)));
                return;
            case "KlappeAuf":
                latch.open();
                return;
            case "KlappeZu":
                latch.close();
                return;
            case "Angle":
                direction.setAngle(parameter);
                return;
            case "Laden":
                reload.reload();
                return;
            case "StepperOn":
                direction.stepperPower(true);
                return;
            case "StepperOff":
                direction.stepperPower(false);
                return;
            case "t":
                testShoot.shoot(Math.toIntExact(Math.round(parameter)));
                return;
            default:
                invalidMessage();
                return;
        }
    }

    private String getCommand(String message) {
        String[] commands = message.split(";");
        if (commands.length > 0) {
            return commands[0];
        } else {
            return "";
        }
    }

    private double getParameter(String message) {
        String[] commands = message.split(";");
        double parameter = 0;

        if (commands.length > 1) {
            try {
                parameter = Double.parseDouble(commands[1]);
            } catch (Throwable t) {
                iosCommunication.sendMessage("Invalid Parameter, using 0");
            }
        }

        return parameter;

    }

    private void addFinishedListener(ListenableFuture<Void> future) {
        future.addListener(new Runnable() {
            @Override
            public void run() {
                iosCommunication.sendMessage("Finished");
            }
        }, executorService);

    }


    private void invalidMessage() {
        iosCommunication.sendMessage("Invalid Message");
    }
}
