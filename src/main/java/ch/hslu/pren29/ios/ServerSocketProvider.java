package ch.hslu.pren29.ios;

import com.google.inject.throwingproviders.CheckedProvider;

import java.io.IOException;

public interface ServerSocketProvider<ServerSocket> extends CheckedProvider<ServerSocket> {
    ServerSocket get() throws IOException;
}