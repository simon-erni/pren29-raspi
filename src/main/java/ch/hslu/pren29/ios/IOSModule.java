package ch.hslu.pren29.ios;

import com.google.inject.AbstractModule;

public class IOSModule extends AbstractModule{

    @Override
    protected void configure() {
        bind(IOSBroker.class);
        bind(IOSCommunication.class);
    }

}
