/*
 * PREN29
 * 
 */
package ch.hslu.pren29.ios;

import com.google.inject.Inject;

import javax.inject.Singleton;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ToDo: Use Handler Threads to accept multiple simultaneous connections.
 *
 * @author Simon
 */
@Singleton
public class IOSCommunication {

    private Thread searchThread;
    private Thread listenerThread;
    private Thread sendMessageThread;

    private ServerSocket serverSocket;

    private OutputStreamWriter out;
    private BufferedReader in;

    private LinkedBlockingDeque<String> messageBuffer;
    private final ArrayList<IOSMessageListener> listener;
    private final Logger logger;

    @Inject
    public IOSCommunication(Logger logger, ServerSocket serverSocket) {
        this.logger = logger;

        this.listener = new ArrayList<>();

        messageBuffer = new LinkedBlockingDeque<>();
        try {
            this.serverSocket = new ServerSocket(1337);
            initConnection();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Could not create ServerSocket", ex);

        }
    }

    /**
     * Sends a specified message to the IOS Device. Async.
     *
     * @param message
     */
    public void sendMessage(String message) {
        putMessageAtStartOfQueue(message + "\r\n");
        logger.log(Level.FINE, "Sending message: " + message);
    }

    public void addMessageListener(IOSMessageListener listener) {
        this.listener.add(listener);
    }

    public void removeMessageListener(IOSMessageListener listener) {
        this.listener.remove(listener);
    }

    private void distributeMessageToListeners(String message) {
        for (IOSMessageListener l : listener) {
            l.messageReceived(message);
        }
    }

    /**
     * Puts the supplied message at the end of the queue. Blocking.
     * ToDo: Handle Interrupted Exception.
     *
     * @param message
     */
    private void putMessageAtEndOfQueue(String message) {
        boolean success = false;
        do {
            try {
                messageBuffer.putLast(message);
                success = true;
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        } while (success == false);
    }

    /**
     * Puts the supplied Message at the Start of the queue. Blocking.
     * ToDo: Handle Interrupted Exception.
     *
     * @param message
     */
    private void putMessageAtStartOfQueue(String message) {
        boolean success = false;
        do {
            try {
                messageBuffer.putFirst(message);
                success = true;
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        } while (!success);
    }

    /**
     * Returns the last Message of the Queue. Blocking.
     * ToDo: Handle Interrupted Exception.
     *
     * @return The last Message
     */
    private String getLastMessageOfQueue() {
        String message = null;
        do {
            try {
                message = messageBuffer.takeLast();
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        } while (message == null);
        return message;
    }

    /**
     * Starts the Connection with the IOS Device, asynchroniously.
     * ToDo: Make this with Future.
     */
    private synchronized void initConnection() {
        if (searchThread == null || !searchThread.isAlive()) {
            searchThread = new Thread(new Runnable() {

                @Override
                public void run() {
                    boolean successfullyConnected = false;
                    do {
                        try {
                            Socket socket = IOSCommunication.this.serverSocket.accept();
                            IOSCommunication.this.out = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
                            IOSCommunication.this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                            successfullyConnected = true;
                        } catch (IOException ex) {
                            logger.log(Level.WARNING, "Fehler beim Verbindungsaufbau", ex);
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                return;
                            }
                        }
                    } while (successfullyConnected == false);
                    IOSCommunication.this.connectionEstablished();
                }
            });
            searchThread.start();
        }

    }

    /**
     * Called when the connection to the IOS Device has been successfully
     * established. Starts the async device listeners and senders.
     */
    private void connectionEstablished() {
        startDeviceListener();
        startDeviceSender();
    }

    private synchronized void startDeviceSender() {
        if (sendMessageThread == null || !sendMessageThread.isAlive()) {
            sendMessageThread = new Thread(new Runnable() {

                @Override
                public void run() {
                    while (true) {
                        String message = getLastMessageOfQueue();
                        try {
                            IOSCommunication.this.out.write(message);
                            IOSCommunication.this.out.flush();
                        } catch (Exception ex) {
                            putMessageAtEndOfQueue(message);
                            initConnection();
                            return;
                        }
                    }
                }
            });
            sendMessageThread.start();
        }
    }

    private synchronized void startDeviceListener() {
        if (listenerThread == null || !listenerThread.isAlive()) {

            listenerThread = new Thread(new Runnable() {

                @Override
                public void run() {
                    while (true) {
                        try {
                            String currentString = IOSCommunication.this.in.readLine();
                            if (currentString != null) {
                                distributeMessageToListeners(currentString);
                            } else {
                                throw new IOException();
                            }
                        } catch (IOException ex) {
                            initConnection();
                            return;
                        }

                    }
                }
            });

            listenerThread.start();
        }
    }

}
