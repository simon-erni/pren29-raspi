/*
 * PREN29
 * 
 */
package ch.hslu.pren29.ios;

/**
 *
 * @author Simon
 */
public interface IOSMessageListener {
    public void messageReceived(String message);
}
