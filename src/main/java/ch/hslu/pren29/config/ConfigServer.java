/*
 * PREN29
 * 
 */
package ch.hslu.pren29.config;

import ch.hslu.pren29.hardware.camera.Camera;
import ch.hslu.pren29.hardware.camera.RecognitionConfiguration;
import com.google.inject.Inject;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Logger;

/**
 * @author Simon
 */
public class ConfigServer implements Config {

    private final Camera camera;
    private Config stub;
    private Registry registry;
    private final RecognitionConfiguration conf;
    private final Logger logger;

    @Inject
    public ConfigServer(Logger logger, Camera camera, RecognitionConfiguration conf) {
        this.camera = camera;
        this.conf = conf;
        this.logger = logger;
    }

    public void startServer() {
        try {
            System.setProperty("java.rmi.server.hostname", "192.168.29.1");
            LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
            registry = LocateRegistry.getRegistry();

            stub = (Config) UnicastRemoteObject.exportObject(this, 0);
            registry.rebind("config", stub);
            System.out.println("Initialized RMI");
        } catch (RemoteException ex) {
            System.out.println("Failed to initialize RMI Server");
            System.err.println(ex.getMessage());
        }
    }

    @Override
    public byte[] takeImage() throws RemoteException {
        try {
            BufferedImage originalImage = camera.takeImage();

            try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                ImageIO.write(originalImage, "jpg", baos);
                baos.flush();
                return baos.toByteArray();
            }

        } catch (Exception ex) {
            throw new RemoteException(ex.getMessage());
        }

    }

    @Override
    public RecognitionConfiguration getCurrentConfiguration() throws RemoteException {
        return conf;
    }

    @Override
    public void setConfiguration(RecognitionConfiguration remoteConf) throws RemoteException {
        this.conf.refresh(remoteConf);
    }

}
