/*
 * PREN29
 * 
 */
package ch.hslu.pren29.config;

import ch.hslu.pren29.hardware.camera.RecognitionConfiguration;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author Simon
 */
public interface Config extends Remote {

    public RecognitionConfiguration getCurrentConfiguration() throws RemoteException;

    public void setConfiguration(RecognitionConfiguration conf) throws RemoteException;

    byte[] takeImage() throws RemoteException;
}
