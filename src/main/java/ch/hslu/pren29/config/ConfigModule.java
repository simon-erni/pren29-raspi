package ch.hslu.pren29.config;

import com.google.inject.AbstractModule;


public class ConfigModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(ConfigServer.class);
    }
}
