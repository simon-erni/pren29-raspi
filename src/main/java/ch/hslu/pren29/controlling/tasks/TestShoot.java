package ch.hslu.pren29.controlling.tasks;

import ch.hslu.pren29.hardware.components.Latch;
import ch.hslu.pren29.hardware.components.Motor;
import ch.hslu.pren29.util.TimeUtils;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;

import java.util.concurrent.Callable;

/**
 * @author Simon
 */
public class TestShoot {

    private final Motor motor;
    private final ListeningExecutorService executorService;
    private final TimeUtils timeUtils;
    private final Latch latch;
    private final Reload reload;

    @Inject
    public TestShoot(Motor motor, TimeUtils timeUtils, ListeningExecutorService executorService, Reload reload, Latch latch) {
        this.motor = motor;
        this.executorService = executorService;
        this.timeUtils = timeUtils;
        this.latch = latch;
        this.reload = reload;
    }

    public ListenableFuture<Void> shoot(final int percentage) {
        return this.executorService.submit(new Callable<Void>() {
            @Override
            public Void call() throws Exception {

                reload.reload().get();
                latch.open().get();
                motor.setPercentage(percentage);

                timeUtils.wait(700);
                motor.setPercentage(0);
                return null;
            }
        });
    }
}
