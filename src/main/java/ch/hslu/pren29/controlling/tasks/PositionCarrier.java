/*
 * PREN29
 * 
 */
package ch.hslu.pren29.controlling.tasks;

import ch.hslu.pren29.hardware.components.Motor;
import ch.hslu.pren29.hardware.components.PositionSensor;
import ch.hslu.pren29.util.TimeUtils;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;

import java.util.concurrent.Callable;

/**
 * ToDo: Test
 * @author Simon
 */
public class PositionCarrier {

    private final Motor motor;
    private final PositionSensor positionSensor;
    private final ListeningExecutorService executorService;
    private final TimeUtils timeUtils;

    @Inject
    public PositionCarrier(Motor motor, PositionSensor positionSensor, ListeningExecutorService executorService, TimeUtils timeUtils) {
        this.motor = motor;
        this.positionSensor = positionSensor;
        this.executorService = executorService;
        this.timeUtils = timeUtils;
    }


    public ListenableFuture<Void> position() {
        return executorService.submit(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                motor.setPercentage(8);
                positionSensor.waitForNoPosition().get();

                timeUtils.wait(100);

                positionSensor.waitForPosition().get();
                motor.setPercentage(0);

                return null;
            }
        });

    }
}
