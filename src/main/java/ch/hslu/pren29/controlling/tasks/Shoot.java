package ch.hslu.pren29.controlling.tasks;

import ch.hslu.pren29.hardware.components.Latch;
import ch.hslu.pren29.hardware.components.Motor;
import ch.hslu.pren29.util.TimeUtils;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;

import java.util.concurrent.Callable;

/**
 * @author Simon
 */
public class Shoot {

    private final Motor motor;
    private final ListeningExecutorService executorService;
    private final TimeUtils timeUtils;
    private final Latch latch;

    @Inject
    public Shoot(Motor motor, TimeUtils timeUtils, ListeningExecutorService executorService, Latch latch) {
        this.motor = motor;
        this.executorService = executorService;
        this.timeUtils = timeUtils;
        this.latch = latch;
    }

    public ListenableFuture<Void> shoot(final double angle) {
        return this.executorService.submit(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                latch.open().get();

                int additional = 0;

                if (Math.abs(angle) > 5)
                {
                    additional = 1;
                }

                motor.setPercentage(60 + additional);

                timeUtils.wait(700);
                motor.setPercentage(0);
                return null;
            }
        });
    }
}
