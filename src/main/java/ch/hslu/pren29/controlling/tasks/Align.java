/*
 * PREN29
 * 
 */
package ch.hslu.pren29.controlling.tasks;

import ch.hslu.pren29.hardware.camera.Camera;
import ch.hslu.pren29.hardware.camera.RecognitionAlgorithm;
import ch.hslu.pren29.hardware.components.Direction;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;

import java.awt.image.BufferedImage;
import java.util.concurrent.Callable;

/**
 * @author Simon
 */
public class Align {

    private final Direction direction;
    private final Camera camera;
    private final ListeningExecutorService executorService;
    private final RecognitionAlgorithm recognitionAlgorithm;


    @Inject
    public Align(Direction direction, Camera camera, ListeningExecutorService executorService, RecognitionAlgorithm recognitionAlgorithm) {

        this.direction = direction;
        this.camera = camera;
        this.executorService = executorService;
        this.recognitionAlgorithm = recognitionAlgorithm;
    }

    public ListenableFuture<Void> align() throws Exception {
        return executorService.submit(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                BufferedImage cameraImage = camera.takeImage();

                double angle = recognitionAlgorithm.getAngle(cameraImage).get();

                direction.setAngle(angle).get();

                return null;
            }
        });

    }
}
