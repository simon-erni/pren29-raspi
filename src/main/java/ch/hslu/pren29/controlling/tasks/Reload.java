/*
 * PREN29
 * 
 */
package ch.hslu.pren29.controlling.tasks;

import ch.hslu.pren29.hardware.components.Hopper;
import ch.hslu.pren29.hardware.components.Latch;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;

import java.util.LinkedList;
import java.util.concurrent.Callable;

/**
 * @author Simon
 */
public class Reload {

    private final Hopper hopper;
    private final Latch latch;
    private final PositionCarrier positionCarrier;
    private final ListeningExecutorService executorService;

    @Inject
    public Reload(Hopper hopper, Latch latch, PositionCarrier positionCarrier, ListeningExecutorService executorService) {
        this.latch = latch;
        this.positionCarrier = positionCarrier;
        this.hopper = hopper;
        this.executorService = executorService;
    }

    public ListenableFuture<Void> reload() {
        return executorService.submit(new Callable<Void>() {
            @Override
            public Void call() throws Exception {

                LinkedList<ListenableFuture<Void>> processList = new LinkedList<>();

                processList.add(latch.close());
                processList.add(positionCarrier.position());

                Futures.allAsList(processList).get();

                hopper.loadBall().get();

                positionCarrier.position().get();

                return null;
            }
        });
    }
}
