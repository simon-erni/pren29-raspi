/*
 * PREN29
 * 
 */
package ch.hslu.pren29.controlling.process;

import ch.hslu.pren29.controlling.services.ContinousAlignService;
import ch.hslu.pren29.controlling.tasks.Reload;
import ch.hslu.pren29.controlling.tasks.Shoot;
import ch.hslu.pren29.hardware.components.Direction;
import ch.hslu.pren29.hardware.components.StatusLEDs;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.concurrent.Callable;
import java.util.logging.Logger;

/**
 * @author Simon
 */
@Singleton
public class MainProcess {

    private final Reload reload;
    private final Shoot shoot;
    private final Direction direction;
    private final StatusLEDs statusLEDs;

    private final ContinousAlignService currentContinousAlignService;

    private final Logger logger;
    private final ListeningExecutorService executorService;

    private ListenableFuture<Void> reloading;
    private ListenableFuture<Void> shootingTask;

    @Inject
    public MainProcess(Logger logger, Reload reload, Shoot shoot, Direction direction, StatusLEDs statusLEDs, ContinousAlignService currentContinousAlignService, ListeningExecutorService executorService) {
        this.reload = reload;
        this.shoot = shoot;
        this.logger = logger;
        this.direction = direction;
        this.statusLEDs = statusLEDs;
        this.currentContinousAlignService = currentContinousAlignService;
        this.executorService = executorService;
    }

    public void prepare() {
        this.direction.stepperPower(true);

        synchronized (this) {

            currentContinousAlignService.start();

            if (this.reloading == null || this.reloading.isDone()) {
                this.reloading = reload.reload();
            }
        }
    }


    public ListenableFuture<Void> shoot(final int count) {

        synchronized (this) {
            if (this.shootingTask == null || this.shootingTask.isDone()) {
                return this.shootingTask = executorService.submit(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {

                        direction.stepperPower(true);

                        statusLEDs.setReadyLED(false);
                        statusLEDs.setRunningLED(true);

                        if (currentContinousAlignService != null) {
                           currentContinousAlignService.stop();
                        }

                        double angle = direction.getCurrentAngle();

                        if (reloading != null) {
                            reloading.get();
                        }

                        for (int i = 0; i < count; i++) {
                            shoot.shoot(angle).get();
                            if (i < count - 1) {
                                reload.reload().get();
                            }
                        }

                        statusLEDs.setRunningLED(false);
                        statusLEDs.setReadyLED(true);

                        direction.stepperPower(false);

                        return null;
                    }
                });
            } else {
                throw new IllegalStateException("Shooting is already running");
            }
        }
    }
}
