package ch.hslu.pren29.controlling;

import ch.hslu.pren29.controlling.process.MainProcess;
import ch.hslu.pren29.controlling.tasks.Align;
import ch.hslu.pren29.controlling.tasks.PositionCarrier;
import ch.hslu.pren29.controlling.tasks.Reload;
import ch.hslu.pren29.controlling.tasks.Shoot;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;

import java.util.concurrent.Executors;

/**
 * Created by Simon on 02.06.2015.
 */
public class ControllingModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(MainProcess.class);
        bind(Align.class);
        bind(PositionCarrier.class);
        bind(Reload.class);
        bind(Shoot.class);

    }

    @Provides
    public ListeningExecutorService provideExecutorService() {
        return MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(3));
    }
}
