package ch.hslu.pren29.controlling.services;

import ch.hslu.pren29.hardware.camera.Camera;
import ch.hslu.pren29.hardware.camera.RecognitionAlgorithm;
import ch.hslu.pren29.hardware.components.Direction;
import ch.hslu.pren29.hardware.components.StatusLEDs;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.inject.Inject;

import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ContinousAlignService implements Runnable {

    private final RecognitionAlgorithm recognitionAlgorithm;
    private final Camera camera;
    private final Direction direction;
    private final Logger logger;

    private double angle;

    private boolean stopped;

    private Thread currentExecution;

    private ListenableFuture<Void> currentAngleFuture;

    private final StatusLEDs statusLEDs;

    @Inject
    public ContinousAlignService(Logger logger, RecognitionAlgorithm recognitionAlgorithm, Camera camera, Direction direction, StatusLEDs statusLEDs) {
        this.recognitionAlgorithm = recognitionAlgorithm;
        this.camera = camera;
        this.direction = direction;
        this.logger = logger;
        this.statusLEDs = statusLEDs;

        stopped = true;
    }

    @Override
    public void run() {
        while (true) {
            try {
                BufferedImage cameraImage = camera.takeImage();

                this.angle = recognitionAlgorithm.getAngle(cameraImage).get();

                currentAngleFuture = direction.setAngle(angle);
                synchronized (this) {
                    if (stopped) {
                        currentAngleFuture.get();
                        return;
                    }
                }
            } catch (Exception ex) {
                logger.log(Level.WARNING, "Could not Align", ex);
                statusLEDs.blinkError(5);
                if (stopped) {
                    return;
                }
            }
        }
    }


    public void start() {
        synchronized (this) {
            if (currentExecution == null || !currentExecution.isAlive()) {
                this.stopped = false;
                currentExecution = new Thread(this);
                currentExecution.start();

            }
        }
    }

    /**
     * Stops the Service.
     *
     * @return
     */
    public void stop() {
        if (this.currentExecution != null) {
            synchronized (this) {
                this.stopped = true;
            }
            try {
                this.currentExecution.join();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
