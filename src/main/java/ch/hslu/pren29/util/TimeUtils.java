package ch.hslu.pren29.util;

/**
 * Created by Simon on 01.06.2015.
 */
public class TimeUtils {

    public void wait(int ms) throws InterruptedException {
        Thread.sleep(ms);
    }

}
