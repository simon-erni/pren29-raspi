package ch.hslu.pren29.util;

import com.google.inject.AbstractModule;

public class UtilsModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(TimeUtils.class);
    }
}
