package ch.hslu.pren29;

import ch.hslu.pren29.config.ConfigModule;
import ch.hslu.pren29.config.ConfigServer;
import ch.hslu.pren29.controlling.ControllingModule;
import ch.hslu.pren29.hardware.RealHardwareModule;
import ch.hslu.pren29.ios.IOSBroker;
import ch.hslu.pren29.ios.IOSModule;
import ch.hslu.pren29.util.UtilsModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * @author Simon
 */
public class Main {

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(
                new IOSModule(),
                new RealHardwareModule(),
                new UtilsModule(),
                new ControllingModule(),
                new ConfigModule()
        );

        IOSBroker iosBroker = injector.getInstance(IOSBroker.class);
        ConfigServer configServer = injector.getInstance(ConfigServer.class);
        configServer.startServer();

    }
}
