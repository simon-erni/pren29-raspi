package ch.hslu.pren29.hardware.components;

import ch.hslu.pren29.hardware.communication.PwmOut;
import ch.hslu.pren29.util.TimeUtils;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import junit.framework.TestCase;

import static org.mockito.Matchers.anyFloat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class LatchTest extends TestCase {

    private Latch latch;
    private PwmOut pwmOut;
    private TimeUtils timeUtils;

    public void setUp() throws Exception {
        super.setUp();

        pwmOut = mock(PwmOut.class);
        timeUtils = mock(TimeUtils.class);

        ListeningExecutorService listeningExecutorService = MoreExecutors.sameThreadExecutor();
        latch = new Latch(pwmOut, timeUtils, listeningExecutorService);
    }

    public void testClose() throws Exception {
        latch.close().get();
        verify(pwmOut, atLeastOnce()).pwmOut(anyInt(), anyFloat());

    }

    public void testOpen() throws Exception {
        latch.open().get();
        verify(pwmOut, atLeastOnce()).pwmOut(anyInt(), anyFloat());

    }

}