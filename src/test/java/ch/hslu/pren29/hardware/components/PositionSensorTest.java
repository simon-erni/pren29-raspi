package ch.hslu.pren29.hardware.components;

import com.google.common.util.concurrent.ListenableFuture;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.logging.Logger;

import static org.mockito.Mockito.when;

public class PositionSensorTest {

    @Mock
    GpioController gpioController;

    @Mock
    Logger logger;

    @Mock
    GpioPinDigitalInput positionSensorPin;

    @Mock
    GpioPinDigitalStateChangeEvent event;

    private PositionSensor positionSensor;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        when(gpioController.provisionDigitalInputPin(RaspiPin.GPIO_21, PinPullResistance.PULL_UP)).thenReturn(positionSensorPin);

        this.positionSensor = new PositionSensor(gpioController, logger);
    }

    @Test
    public void testWaitForPosition() throws Exception {
        ListenableFuture<Void> future = positionSensor.waitForPosition();

        when(event.getState()).thenReturn(PinState.LOW);

        positionSensor.handleGpioPinDigitalStateChangeEvent(event);

        assert (future.isDone());
    }

    @Test
    public void testWaitForNoPosition() throws Exception {
        ListenableFuture<Void> future = positionSensor.waitForNoPosition();

        when(event.getState()).thenReturn(PinState.HIGH);

        positionSensor.handleGpioPinDigitalStateChangeEvent(event);

        assert (future.isDone());
    }

    @Test
    public void testWithMultipleInvocationsNoPosition() throws Exception {
        ListenableFuture<Void> future = positionSensor.waitForNoPosition();

        when(event.getState()).thenReturn(PinState.HIGH);
        positionSensor.handleGpioPinDigitalStateChangeEvent(event);
        positionSensor.handleGpioPinDigitalStateChangeEvent(event);

        assert (future.isDone());
    }

    @Test
    public void testWithMultipleInvocationsAtPosition() throws Exception {
        ListenableFuture<Void> future = positionSensor.waitForPosition();

        when(event.getState()).thenReturn(PinState.LOW);
        positionSensor.handleGpioPinDigitalStateChangeEvent(event);
        positionSensor.handleGpioPinDigitalStateChangeEvent(event);

        assert (future.isDone());
    }

    @Test
    public void testWithMultipleFromOtherState() throws Exception {
        ListenableFuture<Void> future = positionSensor.waitForNoPosition();
        assert (future.isDone() == false);

        when(event.getState()).thenReturn(PinState.LOW);
        positionSensor.handleGpioPinDigitalStateChangeEvent(event);
        positionSensor.handleGpioPinDigitalStateChangeEvent(event);

        assert (future.isDone() == false);


        when(event.getState()).thenReturn(PinState.HIGH);
        positionSensor.handleGpioPinDigitalStateChangeEvent(event);

        assert (future.isDone());
    }

    @Test
    public void testWithMultipleFromOtherStateNoPosition() throws Exception {
        ListenableFuture<Void> future = positionSensor.waitForPosition();
        assert (future.isDone() == false);

        when(event.getState()).thenReturn(PinState.HIGH);
        positionSensor.handleGpioPinDigitalStateChangeEvent(event);
        positionSensor.handleGpioPinDigitalStateChangeEvent(event);

        assert (future.isDone() == false);


        when(event.getState()).thenReturn(PinState.LOW);
        positionSensor.handleGpioPinDigitalStateChangeEvent(event);

        assert (future.isDone());
    }


}