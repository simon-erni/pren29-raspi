package ch.hslu.pren29.hardware.components;

import ch.hslu.pren29.util.TimeUtils;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

public class StatusLEDsTest {

    @Mock
    GpioController gpioController;

    @Mock
    GpioPinDigitalOutput readyLED;

    @Mock
    GpioPinDigitalOutput runningLED;

    @Mock
    GpioPinDigitalOutput errorLED;

    @Mock
    TimeUtils timeUtils;

    ListeningExecutorService executorService;

    StatusLEDs statusLEDs;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        executorService = MoreExecutors.sameThreadExecutor();

        when(gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_02)).thenReturn(readyLED);
        when(gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_09)).thenReturn(runningLED);
        when(gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_08)).thenReturn(errorLED);

        statusLEDs = new StatusLEDs(gpioController, executorService, timeUtils);
    }

    @Test
    public void testInitLEDs() throws Exception {
        verify(readyLED, times(1)).high();
        verify(errorLED, times(1)).low();
        verify(runningLED, times(1)).low();
    }

    @Test
    public void testSetReadyLEDOn() throws Exception {
        statusLEDs.setReadyLED(true);

        InOrder inOrder = inOrder(readyLED);

        inOrder.verify(readyLED, times(2)).high();
        inOrder.verify(readyLED, never()).low();
        inOrder.verifyNoMoreInteractions();

    }

    @Test
    public void testSetReadyLEDOff() throws Exception {
        statusLEDs.setReadyLED(false);

        InOrder inOrder = inOrder(readyLED);

        inOrder.verify(readyLED, times(1)).high();
        inOrder.verify(readyLED, times(1)).low();
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void testSetErrorLEDOn() throws Exception {
        statusLEDs.setErrorLED(true);

        InOrder inOrder = inOrder(errorLED);

        inOrder.verify(errorLED, times(1)).low();
        inOrder.verify(errorLED, times(1)).high();
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void testSetErrorLEDOff() throws Exception {
        statusLEDs.setErrorLED(false);

        InOrder inOrder = inOrder(errorLED);

        inOrder.verify(errorLED, times(2)).low();
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void testSetRunningLEDOn() throws Exception {
        statusLEDs.setRunningLED(true);

        InOrder inOrder = inOrder(runningLED);

        inOrder.verify(runningLED, times(1)).low();
        inOrder.verify(runningLED, times(1)).high();
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void testSetRunningLEDOff() throws Exception {
        statusLEDs.setRunningLED(false);

        InOrder inOrder = inOrder(runningLED);

        inOrder.verify(runningLED, times(2)).low();
        inOrder.verifyNoMoreInteractions();
    }
}