package ch.hslu.pren29.hardware.components;

import ch.hslu.pren29.hardware.communication.PwmOut;
import ch.hslu.pren29.util.TimeUtils;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import junit.framework.TestCase;

import java.util.logging.Logger;

import static org.mockito.Matchers.anyFloat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class HopperTest extends TestCase {

    private PwmOut pwmOut;
    private Hopper hopper;
    private ListeningExecutorService listeningExecutorService;
    private TimeUtils timeUtils;
    private Logger logger;

    public void setUp() throws Exception {
        super.setUp();

        logger = mock(Logger.class);
        pwmOut = mock(PwmOut.class);
        timeUtils = mock(TimeUtils.class);

        listeningExecutorService = MoreExecutors.sameThreadExecutor();


        hopper = new Hopper(logger, pwmOut, timeUtils, listeningExecutorService);
    }

    public void tearDown() throws Exception {
        listeningExecutorService.shutdownNow();
    }

    public void testLoadBall() throws Exception {
        hopper.loadBall().get();
        verify(pwmOut, atLeastOnce()).pwmOut(eq(18), anyFloat());
    }
}