package ch.hslu.pren29.hardware.components;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.CancellationException;
import java.util.concurrent.Executors;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


/**
 * ToDo: Test with Cancel.
 */
public class DirectionTest {

    @Mock
    private GpioPinDigitalOutput stepperPin;

    @Mock
    private GpioPinDigitalOutput stepperPowerPin;

    @Mock
    private GpioPinDigitalOutput stepperDirectionPin;

    @Mock
    private GpioController gpioController;

    private ListeningExecutorService listeningExecutorService;

    private Direction direction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        listeningExecutorService = MoreExecutors.sameThreadExecutor();

        when(gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_29)).thenReturn(stepperPin);
        when(gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_22)).thenReturn(stepperDirectionPin);
        when(gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_14)).thenReturn(stepperPowerPin);

        direction = new Direction(listeningExecutorService, gpioController);
    }

    @After
    public void tearDown() throws Exception {
        listeningExecutorService.shutdownNow();
    }

    @Test
    public void testStepperPowerOn() throws Exception {
        direction.stepperPower(true);
        verify(stepperPowerPin, times(1)).low();
    }

    @Test
    public void testStepperPowerOff() throws Exception {
        direction.stepperPower(false);
        verify(stepperPowerPin).high();
    }

    @Test
    public void testSetAngleZero() throws Exception {
        direction.setAngle(0).get();
        verifyZeroInteractions(stepperPin);
    }

    @Test
    public void testSetAngle() throws Exception {
        direction.setAngle(10).get();

        verify(stepperDirectionPin, times(1)).low();
        verify(stepperPin, times(1067)).high();
        verify(stepperPin, times(1067)).low();
    }

    @Test
    public void testSetDirectionPositive() throws Exception {
        direction.setAngle(10).get();

        verify(stepperDirectionPin).low();

    }

    @Test
    public void testSetDirectionNegative() throws Exception {


        direction.setAngle(-10).get();

        verify(stepperDirectionPin, times(2)).high();

    }

    @Test
    public void testMultipleInvocations() throws Exception {
        listeningExecutorService.shutdownNow();
        listeningExecutorService = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());

        direction = new Direction(listeningExecutorService, gpioController);

        ListenableFuture<Void> first = direction.setAngle(5);
        ListenableFuture<Void> second = direction.setAngle(-5);

        try {
            first.get();
        } catch (CancellationException ex) {

        }
        second.get();
        assertEquals(-5, direction.getCurrentAngle(), 0.0d);

    }
}