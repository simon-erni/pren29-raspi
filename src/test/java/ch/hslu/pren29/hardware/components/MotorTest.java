package ch.hslu.pren29.hardware.components;

import ch.hslu.pren29.hardware.communication.PwmOut;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.logging.Logger;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class MotorTest {

    @Mock
    private PwmOut pwmOut;

    @Mock
    private GpioController gpioController;

    @Mock
    private GpioPinDigitalOutput motorBreakPin;

    @Mock
    private Logger logger;

    private Motor motor;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        when(gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_23)).thenReturn(motorBreakPin);

        this.motor = new Motor(logger, pwmOut, gpioController);
    }

    @Test
    public void testSetFullPercentage() throws Exception {
        motor.setPercentage(100);

        verify(pwmOut, atLeastOnce()).pwmOut(anyInt(), eq(1f));
    }


    @Test
    public void testSetZeroPercentage() throws Exception {
        motor.setPercentage(0);

        verifyZeroInteractions(pwmOut);
    }

    @Test
    public void testSetAnyPercentage() throws Exception {
        motor.setPercentage(33);

        verify(pwmOut, atLeastOnce()).pwmOut(anyInt(), eq(0.33f));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetNegativePercentage() throws Exception {
        motor.setPercentage(-90);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetTooHighPercentage() throws Exception {
        motor.setPercentage(101);
    }

}