package ch.hslu.pren29.hardware.communication;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.Writer;
import java.util.logging.Logger;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

public class PwmOutTest {

    @Mock
    Writer writer;

    @Mock
    Logger logger;

    PwmOut pwmOut;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        pwmOut = new PwmOut(logger, writer);
    }

    @Test
    public void testPwmOut() throws Exception {
        pwmOut.pwmOut(4, 0.4f);
        verify(writer).write(eq("4=0.4\n"));
        verify(writer).flush();
    }

    @Test
    public void testReleasePWM() throws Exception {
        pwmOut.releasePWM(4);
        verifyZeroInteractions(writer);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPwmOutInvalidPin() throws Exception {
        pwmOut.pwmOut(984, 0.4f);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPwmOutNegativeValue() throws Exception {
        pwmOut.pwmOut(4, -0.4f);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPwmOutTooBigValue() throws Exception {
        pwmOut.pwmOut(4, 6845165854f);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReleasePWMNegativePin() throws Exception {
        pwmOut.releasePWM(-9000);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReleasePWMInvalidPin() throws Exception {
        pwmOut.releasePWM(698465468);
    }

    @Test
    public void testPwmOutZero() throws Exception {
        pwmOut.pwmOut(4, 0f);
        verify(writer).write(eq("4=0\n"));
        verify(writer).flush();
    }
}