/*
 * PREN29
 * 
 */
package ch.hslu.pren29.hardware.camera;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;

/**
 * @author Simon
 */
public class RecognitionAlgorithmTest {

    private RecognitionAlgorithm algo;

    @Mock
    private Logger logger;

    ListeningExecutorService executorService;

    private static RecognitionConfiguration oldConfiguration;

    private RecognitionConfiguration configuration;

    @BeforeClass
    public static void setUpClass() {
        oldConfiguration = new RecognitionConfiguration();
    }

    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        executorService = MoreExecutors.sameThreadExecutor();

        configuration = new RecognitionConfiguration();


        algo = new RecognitionAlgorithm(logger, executorService, configuration);
    }

    @AfterClass
    public static void tearDownClass() {
        oldConfiguration.refresh(oldConfiguration);
    }

    /**
     * Test of getKorbLocation method, of class RecognitionAlgorithm.
     *
     * @throws java.lang.Exception When a failure from the Recognition Algorithm
     *                             happens.
     */
    @Test
    public void testRecognizeSimple() throws Exception {

        configuration.setMaxKorbLuminancy(40);
        configuration.setMinKorbWidth(50);
        configuration.setMaxKorbWidth(60);
        configuration.setEdgeDetectingWidth(5);
        configuration.setEdgeHeight(50);

        int[] testData = new int[200];

        /**
         * First 50 are too high (= 100)
         *
         */
        for (int i = 0; i < 50; i++) {
            testData[i] = 100;
        }

        for (int i = 50; i < 100; i++) {
            testData[i] = 40;
        }

        for (int i = 100; i < 200; i++) {
            testData[i] = 250;
        }

        KorbLocation location = algo.getKorbLocation(testData);


        assertEquals(50, location.getHorizontalStart());
        assertEquals(100, location.getHorizontalEnd());
    }

    @Test
    public void testKorbOverlapsImageRight() throws Exception {

        configuration.setMaxKorbLuminancy(40);
        configuration.setMinKorbWidth(50);
        configuration.setMaxKorbWidth(55);
        configuration.setEdgeDetectingWidth(5);
        configuration.setEdgeHeight(50);

        int[] testData = new int[200];

        /**
         * First 180 are too high (= 100)
         *
         */
        for (int i = 0; i < 180; i++) {
            testData[i] = 100;
        }

        /**
         * The rest is the basket, indicating that the Basket is 'out of sight'.
         */

        for (int i = 180; i < testData.length; i++) {
            testData[i] = 40;
        }

        KorbLocation location = algo.getKorbLocation(testData);


        assertEquals(180, location.getHorizontalStart());
        assertEquals(180 + 55, location.getHorizontalEnd());


    }

    @Test
    public void testKorbOverlapsImageLeft() throws Exception {

        configuration.setMaxKorbLuminancy(40);
        configuration.setMinKorbWidth(20);
        configuration.setMaxKorbWidth(55);
        configuration.setEdgeDetectingWidth(5);
        configuration.setEdgeHeight(50);

        int[] testData = new int[200];

        /**
         * First 20 are ok  (= 40)
         *
         */
        for (int i = 0; i < 20; i++) {
            testData[i] = 40;
        }

        /**
         * The rest is too high, indicating that the Basket is 'out of sight' on the left.
         */

        for (int i = 20; i < testData.length; i++) {
            testData[i] = 100;
        }

        KorbLocation location = algo.getKorbLocation(testData);

        assertEquals(20 - 55, location.getHorizontalStart());
        assertEquals(20, location.getHorizontalEnd());


    }

    @Test
    public void testKorbWithBlackLeft() throws Exception {

        configuration.setMaxKorbLuminancy(40);
        configuration.setMinKorbWidth(50);
        configuration.setMaxKorbWidth(55);
        configuration.setEdgeDetectingWidth(5);
        configuration.setEdgeHeight(50);

        int[] testData = new int[200];

        /**
         * First 70 is black
         *
         */
        for (int i = 0; i < 70; i++) {
            testData[i] = 40;
        }

        for (int i = 70; i < 180; i++) {
            testData[i] = 100;
        }

        for (int i = 180; i < 200; i++) {
            testData[i] = 20;
        }


        KorbLocation location = algo.getKorbLocation(testData);


        assertEquals(70 - 55, location.getHorizontalStart());
        assertEquals(70, location.getHorizontalEnd());


    }

    @Test
    public void testKorbWithBlackRight() throws Exception {

        configuration.setMaxKorbLuminancy(40);
        configuration.setMinKorbWidth(50);
        configuration.setMaxKorbWidth(55);
        configuration.setEdgeDetectingWidth(5);
        configuration.setEdgeHeight(50);

        int[] testData = new int[200];


        /**
         * All high.
         */
        for (int i = 0; i < 200; i++) {
            testData[i] = 100;
        }

        /**
         * First 20 is black
         *
         */
        for (int i = 0; i < 20; i++) {
            testData[i] = 40;
        }

        /**
         * Last 20 black.
         */

        for (int i = 180; i < 200; i++) {
            testData[i] = 20;
        }


        /**
         * Basket at 130 - 185
         */
        for (int i = 130; i < 185; i++) {
            testData[i] = 40;
        }




        KorbLocation location = algo.getKorbLocation(testData);

        assertEquals(130, location.getHorizontalStart());
        assertEquals(185, location.getHorizontalEnd());


    }


    @Test
    public void testKorbWithBlackEverywhere() throws Exception {

        configuration.setMaxKorbLuminancy(40);
        configuration.setMinKorbWidth(50);
        configuration.setMaxKorbWidth(55);
        configuration.setEdgeDetectingWidth(5);
        configuration.setEdgeHeight(50);

        int[] testData = new int[500];


        /**
         * All high.
         */
        for (int i = 0; i < 500; i++) {
            testData[i] = 100;
        }

        /**
         * First 20 is black
         *
         */
        for (int i = 0; i < 20; i++) {
            testData[i] = 40;
        }

        /**
         * Last 20 black.
         */

        for (int i = 480; i < 500; i++) {
            testData[i] = 20;
        }




        /**
         * Random blacks.
         */
        for (int i = 200; i < 225; i++) {
            testData[i] = 40;
        }

        for (int i = 40; i < 60; i ++) {
            testData[i] = 40;
        }

        /**
         * Basket at 130 - 185
         */
        for (int i = 130; i < 185; i++) {
            testData[i] = 40;
        }

        /**
         * A bit of shadow just over the limit.
         */

        for (int i = 185; i < 260; i++) {
            testData[i] = 45;
        }


        KorbLocation location = algo.getKorbLocation(testData);

        assertEquals(130, location.getHorizontalStart());
        assertEquals(185, location.getHorizontalEnd());


    }


    @Test
    public void testKorbWithTooMuchBlack() throws Exception {

        configuration.setMaxKorbLuminancy(40);
        configuration.setMinKorbWidth(40);
        configuration.setMaxKorbWidth(50);
        configuration.setEdgeDetectingWidth(5);
        configuration.setEdgeHeight(50);

        int[] testData = new int[500];


        /**
         * All high.
         */
        for (int i = 0; i < 500; i++) {
            testData[i] = 100;
        }


        /**
         * Basket at 100 - 200
         */
        for (int i = 100; i < 200; i++) {
            testData[i] = 40;
        }


        KorbLocation location = algo.getKorbLocation(testData);


        assertEquals(125, location.getHorizontalStart());
        assertEquals(175, location.getHorizontalEnd());


    }

    @Test
    public void testPixelToAngleZero() throws Exception {
        assertEquals(0, algo.pixelToAngle(320, 640), 0);
    }

    @Test
    public void testPixelToAngleConsistency() throws Exception {
        assertEquals(0, algo.pixelToAngle(600, 640) + algo.pixelToAngle(40, 640), 0);
    }

    @Test
    public void testPixelToAngleLeft() throws Exception {
        assertEquals(15.255, algo.pixelToAngle(20, 640), 0.1d);
    }

    @Test
    public void testPixelToAngleRight() throws Exception {
        assertEquals(-16.22, algo.pixelToAngle(640, 640), 0.1d);
    }

    @Test
    public void testPixelToAngleNegative() throws Exception {
        assertEquals(17.65, algo.pixelToAngle(-30, 640), 0.1d);
    }

    @Test
    public void testPixelToAnglePositive() throws Exception {
        assertEquals(-17.17, algo.pixelToAngle(660, 640), 0.1d);
    }
}
