package ch.hslu.pren29.controlling.services;

import ch.hslu.pren29.controlling.tasks.Align;
import ch.hslu.pren29.hardware.camera.Camera;
import ch.hslu.pren29.hardware.camera.RecognitionAlgorithm;
import ch.hslu.pren29.hardware.components.Direction;
import ch.hslu.pren29.hardware.components.StatusLEDs;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.awt.image.BufferedImage;
import java.util.logging.Logger;

import static org.mockito.Mockito.*;

public class ContinousAlignServiceTest {

    @Mock
    Logger logger;

    @Mock
    private Align align;

    @Mock
    private RecognitionAlgorithm recognitionAlgorithm;

    @Mock
    private Camera camera;

    @Mock
    private Direction direction;

    @Mock
    private BufferedImage bufferedImage;

    @Mock
    private StatusLEDs statusLEDs;

    private ContinousAlignService continousAlignService;

    @Mock
    private ListenableFuture<Void> directionFuture;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        SettableFuture<Double> recognitionFuture = SettableFuture.create();
        recognitionFuture.set(200d);

        SettableFuture<BufferedImage> imageFuture = SettableFuture.create();
        imageFuture.set(bufferedImage);

        when(camera.takeImage()).thenReturn(bufferedImage);
        when(recognitionAlgorithm.getAngle(any(BufferedImage.class))).thenReturn(recognitionFuture);
        when(direction.setAngle(anyDouble())).thenReturn(directionFuture);

        continousAlignService = new ContinousAlignService(logger, recognitionAlgorithm, camera, direction, statusLEDs);
    }

    @After
    public void tearDown() throws Exception {
        continousAlignService.stop();
    }

    @Test
    public void testCompleteAtLeastOnce() throws Exception {
        continousAlignService.start();
        continousAlignService.stop();

        verify(camera, atLeastOnce()).takeImage();
        verify(recognitionAlgorithm, atLeastOnce()).getAngle(bufferedImage);
        verify(direction, atLeastOnce()).setAngle(200d);
        verify(directionFuture, atLeastOnce()).get();

    }
}