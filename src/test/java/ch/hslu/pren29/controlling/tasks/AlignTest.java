package ch.hslu.pren29.controlling.tasks;

import ch.hslu.pren29.hardware.camera.Camera;
import ch.hslu.pren29.hardware.camera.RecognitionAlgorithm;
import ch.hslu.pren29.hardware.components.Direction;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.SettableFuture;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.awt.image.BufferedImage;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AlignTest {

    private ListeningExecutorService listeningExecutorService;

    @Mock
    Direction direction;

    @Mock
    Camera camera;

    @Mock
    RecognitionAlgorithm recognitionAlgorithm;

    @Mock
    BufferedImage image;

    Align align;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        listeningExecutorService = MoreExecutors.sameThreadExecutor();

        when(camera.takeImage()).thenReturn(image);
        when(recognitionAlgorithm.getAngle(image)).thenReturn(Futures.immediateFuture(0d));

        SettableFuture<Void> voidFuture = SettableFuture.create();
        voidFuture.set(null);

        when(direction.setAngle(0d)).thenReturn(voidFuture);

        align = new Align(direction,camera,listeningExecutorService,recognitionAlgorithm);
    }

    @After
    public void tearDown() throws Exception {
        listeningExecutorService.shutdownNow();
    }

    @Test
    public void testAlign() throws Exception {

        align.align();

        verify(camera).takeImage();
        verify(recognitionAlgorithm).getAngle(image);
        verify(direction).setAngle(0d);
    }
}