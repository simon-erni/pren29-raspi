package ch.hslu.pren29.controlling.tasks;

import ch.hslu.pren29.hardware.components.Motor;
import ch.hslu.pren29.hardware.components.PositionSensor;
import ch.hslu.pren29.util.TimeUtils;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.SettableFuture;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.when;

public class PositionCarrierTest {

    @Mock
    private Motor motor;

    @Mock
    private PositionSensor positionSensor;

    private ListeningExecutorService executorService;

    @Mock
    private TimeUtils timeUtils;

    private PositionCarrier positionCarrier;

    private ListenableFuture<Void> finishedFuture;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        SettableFuture<Void> voidFuture = SettableFuture.create();
        voidFuture.set(null);
        this.finishedFuture = voidFuture;
        executorService = MoreExecutors.sameThreadExecutor();
        positionCarrier = new PositionCarrier(motor, positionSensor, executorService, timeUtils);
    }

    @After
    public void tearDown() throws Exception {
        executorService.shutdownNow();
    }

    @Test
    public void testPosition() throws Exception {
        when(positionSensor.waitForNoPosition()).thenReturn(finishedFuture);
        when(positionSensor.waitForPosition()).thenReturn(finishedFuture);

        positionCarrier.position();

        InOrder inOrder = inOrder(motor, positionSensor, timeUtils);

        inOrder.verify(motor).setPercentage(anyInt());
        inOrder.verify(positionSensor).waitForNoPosition();
        inOrder.verify(timeUtils).wait(anyInt());
        inOrder.verify(positionSensor).waitForPosition();
        inOrder.verify(motor).setPercentage(0);
        inOrder.verifyNoMoreInteractions();

    }
}