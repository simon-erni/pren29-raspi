package ch.hslu.pren29.controlling.tasks;

import ch.hslu.pren29.hardware.components.Hopper;
import ch.hslu.pren29.hardware.components.Latch;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.SettableFuture;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ReloadTest {

    @Mock
    private Hopper hopper;
    @Mock
    private Latch latch;
    @Mock
    private PositionCarrier positionCarrier;

    private ListeningExecutorService executorService;

    private ListenableFuture<Void> finishedFuture;

    Reload reload;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        SettableFuture<Void> voidFuture = SettableFuture.create();
        voidFuture.set(null);
        this.finishedFuture = voidFuture;

        executorService = MoreExecutors.sameThreadExecutor();

        when(hopper.loadBall()).thenReturn(finishedFuture);
        when(latch.close()).thenReturn(finishedFuture);
        when(positionCarrier.position()).thenReturn(finishedFuture);


        reload = new Reload(hopper, latch, positionCarrier, executorService);
    }

    @Test
    public void testReload() throws Exception {
        reload.reload();


        verify(latch).close();
        verify(positionCarrier, times(2)).position();
        verify(hopper).loadBall();


    }
}