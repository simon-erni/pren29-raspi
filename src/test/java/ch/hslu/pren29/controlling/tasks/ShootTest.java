package ch.hslu.pren29.controlling.tasks;

import ch.hslu.pren29.hardware.components.Latch;
import ch.hslu.pren29.hardware.components.Motor;
import ch.hslu.pren29.util.TimeUtils;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.SettableFuture;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

public class ShootTest {
    @Mock
    private Motor motor;

    private ListeningExecutorService executorService;

    @Mock
    private TimeUtils timeUtils;

    @Mock
    private Latch latch;

    private Shoot shoot;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        executorService = MoreExecutors.sameThreadExecutor();

        SettableFuture<Void> voidFuture = SettableFuture.create();
        voidFuture.set(null);

        shoot = new Shoot(motor, timeUtils, executorService, latch);

        when(latch.open()).thenReturn(voidFuture);


    }

    @After
    public void tearDown() throws Exception {
        executorService.shutdownNow();
    }

    @Test
    public void testShoot() throws Exception {
        shoot.shoot(0);
        verify(latch).open();
        verify(motor, times(2)).setPercentage(anyInt());
        verify(timeUtils).wait(anyInt());
        verifyNoMoreInteractions(latch, motor, timeUtils);
    }
}