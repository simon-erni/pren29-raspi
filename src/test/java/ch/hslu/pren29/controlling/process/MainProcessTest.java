package ch.hslu.pren29.controlling.process;

import ch.hslu.pren29.controlling.services.ContinousAlignService;
import ch.hslu.pren29.controlling.tasks.Reload;
import ch.hslu.pren29.controlling.tasks.Shoot;
import ch.hslu.pren29.hardware.components.Direction;
import ch.hslu.pren29.hardware.components.StatusLEDs;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.SettableFuture;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.Executors;
import java.util.logging.Logger;

import static org.mockito.Mockito.*;

public class MainProcessTest {

    @Mock
    private ContinousAlignService alignService;

    @Mock
    private Reload reload;

    @Mock
    private Shoot shoot;

    @Mock
    private Logger logger;

    @Mock
    Direction direction;

    @Mock
    StatusLEDs statusLEDs;

    private ListeningExecutorService executorService;

    private ListenableFuture<Void> voidFuture;

    private MainProcess mainProcess;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        executorService = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());
        SettableFuture<Void> voidFuture = SettableFuture.create();
        voidFuture.set(null);

        this.voidFuture = voidFuture;

        when(reload.reload()).thenReturn(voidFuture);
        when(shoot.shoot(0)).thenReturn(voidFuture);

        mainProcess = new MainProcess(logger, reload, shoot, direction, statusLEDs, alignService, executorService);
    }

    @After
    public void tearDown() throws Exception {
        executorService.shutdownNow();
    }

    @Test
    public void testPrepareRuns() throws Exception {
        mainProcess.prepare();

        verify(alignService).start();
        verify(reload).reload();
    }

    @Test
    public void testPrepareTwoTimesReloadNotDone() throws Exception {
        ListenableFuture<Void> reloadFuture;
        reloadFuture = SettableFuture.create();

        when(reload.reload()).thenReturn(reloadFuture);
        mainProcess.prepare();
        mainProcess.prepare();

        verify(alignService, times(2)).start();
        verify(reload, times(1)).reload();

    }

    @Test
    public void testShoot() throws Exception {
        ListenableFuture<Void> shootingTask = mainProcess.shoot(5);

        shootingTask.get();
        verify(shoot, times(5)).shoot(0);
        verify(reload, times(4)).reload();

    }


    @Test(expected = IllegalStateException.class)
    public void testShootTwoTimesShootNotDone() throws Exception {
        ListenableFuture<Void> reloadFuture;
        reloadFuture = SettableFuture.create();

        when(reload.reload()).thenReturn(reloadFuture);

        mainProcess.shoot(5);

        mainProcess.shoot(5);

    }

    @Test
    public void testWaitForReloading() throws Exception {
        ListenableFuture<Void> reloadMock = mock(ListenableFuture.class);

        when(reload.reload()).thenReturn(reloadMock);

        mainProcess.prepare();
        mainProcess.shoot(0).get();

        verify(reloadMock, times(1)).get();

    }


}